# GoodData refresher

## Configuration

See `config.schema.json` or [json-editor](https://json-editor.github.io/json-editor/?data=N4Ig9gDgLglmB2BnEAuUMDGCA2MBGqIAZglAIYDuApomALZUCsIANOHgFZUZQD62ZAJ5gArlELwwAJzplsrEIgwALKrNSgAJEtXqUIZVCgQUAelMda8ALQ61ZAHTSA5qYAmUskSjWADAHZTO3U2KEEIKkIwTm5xNikqAEcRGAS3VABtEHIpZyooZABdNggpSCopWBoNbLJc/OQ0bJgobEj9ABU6vIKFNxoMKRhoOHhCABkYRCgAAjAiGZyemdKwLh4ZmDdERbAZhKIExGUFMIjCOs9BBRa1RtAz9vZ1uJAE5NSqdJQsiC2FESICoKCBkRCICjSdJsNz0MgwMZsOxuERtXj/YogVYRSowapNP7fB7hJ7TIbwZynFptQgABTKLxmAEkACJ9AZDEYIQgAcTAYDcLLI5BWDNimzcu32VEONBOAF82IDgU1HoQyQjKaFqU8AKpAqTspSc2Dc/T6iozeBkBhS7BgZwIqV8gVCkUUFoKkpgiFQmpq/QailU1pPWk+yFSaEgfrG4amsb6cPgyOSqB7e2O+DO/mC4VkGYeqBemNwhH+knqqDkrXNUOEFll7PWhhGwbx0aEAByNqocwWwhEUhmLrzIthsidAAoqA5nA4ZgByIEYIdURczEjDldrue5tz5hxYOgASgUVAAHjaIG1GlkdwkFHgYAAvF/VqjChzWkCFRWKFQvlRKh0SJbJK0DatNRDGl9AAZUAlE2mZNkYQ5DszRAABNUQZgwMhsyIBFJUHbdEOAiVNmzFkmTggBhKiZlImZdQAJXGEB/wPcggV6VUIKyS4hAUeBUXkTFYHrfRJmmfsZm4sEGjbE1O30JkB1w2ErTAWYPWOKURAgbi+zkbB5PzXididZjVheFgZkQCIMBgIhBEWXQZlkUTTOuNhL2vW9MiyBTeKcKMKmQGELPyBwyAwLARHgXpCkxW46HucDzkgmsQWFKAKkTEAAD0QvyAAdMqHAyCqKAqtwKusXgHEKABqTROPlTquqRZQwAoXgKjKKRGhABF8s8HhO1CXQnjwfkCmrMgIAAFg6oA===) for more.

```json
{
    "$schema": "./config.schema.json",
    "targets": [
        {
            "domain": "bizztreat.na",
            "pid": "project id",
            "password": "password",
            "user": "gd username",
            "schedule_id": "schedule id (see below)"
        }
    ]
}
```

### Password

You should not keep your password stored in a file. When possible, use the `GD_PASSWORD` environment variable instead.

### How to get schedule id

When in GoodData Data Integration Console (DISC), your schedule id will be at the end of the url.

![Schedule id](schedule_id.png)
