FROM python:3.8-slim

WORKDIR /code

ADD requirements.txt ./
ADD config.schema.json ./

RUN python -m pip install -r requirements.txt

ADD src/main.py ./

CMD ["python", "-u", "main.py"]
