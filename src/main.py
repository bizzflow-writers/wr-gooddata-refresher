"""Main runner"""

import os
import logging
from bizztreat_base.config import Config
from operator import itemgetter
from gd import GoodData

logger = logging.getLogger(__name__)


def main():
    logger.info("Loading configuration")
    config = Config()
    pwd = os.getenv("GD_PASSWORD", None)
    for target in config.config.get("targets", []):
        user = target["user"]
        password = target.get("password", target.get("#password"))
        domain = target["domain"]
        pid = target["pid"]
        schedule_id = target["schedule_id"]
        password = pwd or password
        if password is None:
            raise ValueError("Password was not specified in config, nor found in env variable GD_PASSWORD")
        logger.info("Processing execution of schedule %s as %s in project %s", schedule_id, user, pid)
        gd = GoodData(user=user, password=password, domain=domain, pid=pid)
        datasets = target.get("datasets")
        if not datasets:  # may be [] or None, we only need None
            datasets = None
        poller = gd.execute_schedule(schedule_id, datasets)
        logger.info("Awaiting task completion...")
        try:
            poller.wait()
        except Exception as error:
            logger.error("Task execution failed. See below for details or visit GoodData DISC for logs.")
            raise error
        logger.info("Refresh success")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
